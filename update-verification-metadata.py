#!/usr/bin/env python3
import os
import json

filename = "gradle/verification-metadata.xml"
with open('../hashes.json') as f:
    hashes = json.load(f)

target = os.getenv("TARGET")

if target not in hashes:
    raise Exception("Target {} not in known hashes, please update the python script".format(os.getenv("TARGET")))

with open(filename) as f:
    template = f.read()

output = template.format(TARGET=target, TARGET_JAR_SHA256=hashes[target]["jar"], TARGET_MODULE_SHA256=hashes[target]["module"])

with open(filename, 'w') as f:
    f.write(output)